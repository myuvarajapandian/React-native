import { useState, useEffect } from "react";
import axios from "axios";

const useFetch = (endpoint, query) => {
    const [ data, setData ] = useState([]);
    const [ isLoading, setIsLoading ] = useState(true);
    const [ error, setError ] = useState(null);


const options = {
  method: 'GET',
  url: `https://jsearch.p.rapidapi.com/${endpoint}`,
  headers: {
    'X-RapidAPI-Key': 'b859600cb6mshf327d2d72852abfp1df327jsn25982321dcc1',
    'X-RapidAPI-Host': 'jsearch.p.rapidapi.com'
  },
  params: { ...query },
};

const fetchData = async () => {
    setIsLoading(true);

    try {
        const response = await axios.request
        (options);
        
        setData(response.data.data);
        setIsLoading(false);
    } catch (error) {
        setError(error);
        // console.log(error);
        alert("there is an error");
    } finally {
        setIsLoading(false);
    }
};

useEffect(() => {
    fetchData();
}, []);

const refetch = () => {
    setIsLoading(true);
    fetchData();
};

return { data, isLoading, error, refetch };
};

export default useFetch;
